extends Control

@export var draggable : Control
@export var droparea : Node
@export var handle : Node

func _process(_delta):
	draggable.position = droparea.snap_to_collision_grid(position - handle.size/2)

func _notification(type):
	if (type == NOTIFICATION_PREDELETE):
		handle.return_to_drag_start()
