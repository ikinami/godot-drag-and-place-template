# Godot Drag and Place Template

This template makes it easier to drag and place items of certain shapes to snap to a pre-defined grid.

Each item can be configured with a custom collisionmap, which shows which areas of the item are occupied, when the item is placed.

![Demo video](drag-and-place-snap-demo.mp4)
