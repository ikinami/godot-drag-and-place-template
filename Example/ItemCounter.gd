extends RichTextLabel

var items = 0

func _on_droparea_item_added(_data):
	items += 1
	text = "Items on grid: %d" % items


func _on_droparea_item_removed(_data):
	items -= 1
	text = "Items on grid: %d" % items
